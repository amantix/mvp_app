﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Tools;

namespace MVP_App.Views
{
    public partial class MainView : Form, IView<List<Note>>
    {
        public MainView()
        {
            InitializeComponent();
        }

        private List<Note> _notes;

        public Action<List<Note>> SetData { get; set; }

        public void UpdateData(List<Note> data)
        {
            _notes = data;

            //draw data on form
        }

        public new void Show()
        {
            Application.Run(this);
        }

    }
}
