﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace MVP_App.Models
{
    public class MainModel : IModel<List<Note>,DateTime>, IModel<List<Note>,string>
    {
        public Model Model { get; private set; }
        public MainModel()
        {

        }

        public MainModel(string filename)
        {
            Model = new Model(filename);
        }

        public MainModel(Model model)
        {
            Model = model;
        }

        public void SetSource(string source)
        {
            Model = new Model(source);
        }

        public List<Note> GetData(DateTime id)
        {
            return Model.Notes.Where(n => n.DateTime.Equals(id)).ToList();
        }

        public List<Note> GetData(string id)
        {
            return Model.Notes.Where(n => n.Text.Contains(id)).ToList();
        }

        public void SaveData(List<Note> data)
        {
            Model.Notes = data;
            Model.Serialize();
        }
    }
}
