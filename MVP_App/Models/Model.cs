﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;
using System.Xml.Serialization;

namespace MVP_App.Models
{
    public class Model
    {
        public List<Note> Notes { get; set; } = new List<Note>();
        //{
        //    new Note {Text="Note1", DateTime=new DateTime(2016,4,1) },
        //    new Note {Text="Note2", DateTime=new DateTime(2016,5,1) },
        //    new Note {Text="Note3", DateTime=new DateTime(2016,6,1) },
        //    new Note {Text="Note4", DateTime=new DateTime(2016,7,1) }
        //};

        public Model(string filename)
        {
            _filename = filename;
            using (var fs = new FileStream(_filename, FileMode.Open))
            {
                var ser = new XmlSerializer(typeof(List<Note>));
                Notes=ser.Deserialize(fs ) as List<Note>;
            }
        }

        public Model(string filename, List<Note> notes)
        {
            _filename = filename;
            Notes = notes;
        }

        private readonly string _filename;

        public void Serialize()
        {
            using (var fs= new FileStream(_filename, FileMode.Create))
            {
                var ser = new XmlSerializer(typeof(List<Note>));
                ser.Serialize(fs, Notes);
            }

        }     
    }
}
