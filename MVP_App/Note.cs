﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVP_App
{
    public class Note
    {
        public string Text { get; set; }
        public DateTime DateTime { get; set; }
    }
}
