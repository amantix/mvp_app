﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;

namespace MVP_App.Presenters
{
    public class MainPresenter : IPresenter<List<Note>,List<Note>,string>
    {
        public IModel<List<Note>, string> Model { get; }
        public IView<List<Note>> View { get; }

        public MainPresenter(IModel<List<Note>,string> model, IView<List<Note>> view)
        {
            Model = model;
            View = view;
            this.RegisterView();
        }

        public List<Note> GetData(string id)
        {
            return Model.GetData(id);
        }

        public void SaveData(List<Note> data)
        {
            Model.SaveData(data);
        }

        public void Run()
        {
            View.Show();
        }
    }
}
