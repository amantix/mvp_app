﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using MVP_App.Models;
using MVP_App.Presenters;
using MVP_App.Views;

namespace MVP_App
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var model = new MainModel("notes.xml");
            var mainView = new MainView();

            var mainPresenter = new MainPresenter(model, mainView);

            //ApplicationContext context = new ApplicationContext();
            mainPresenter.Run();

        }
    }
}
