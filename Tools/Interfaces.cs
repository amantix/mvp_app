﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
    /*
     * 3 категории mvp
    -Passive View: View содержит минимальную логику отображения примитивных данных(строки, числа), остальным занимается Presenter;
    -Presentation Model: во View могут передаваться не только примитивные данные, но и бизнес-объекты;
    -Supervising Controller: View знает о наличии модели и сам забирает из нее данные.
    */
    
    
    public interface IModel<TData, TId>
    {
        void SetSource(string source);
        TData GetData(TId id);
        void SaveData(TData data);
    }  

    public interface IPresenter<TData, TDto, TId>
    {
        IModel<TData,TId> Model { get; }
        IView<TDto> View { get; }
        TData GetData(TId id);
        void SaveData(TDto data);
        void Run();
    }

    public static class PresenterExtension
    {
        public static void RegisterView<TData,TDto,TId>(this IPresenter<TData,TDto,TId> presenter)
        {
            presenter.View.SetData += presenter.SaveData;
        }
    }

    public interface IView<TDto>
    {
        // Presenter подписывается на View (необходимость сохранения данных)
        Action<TDto> SetData { get; set; }

        // Presenter вызывает обновление данных на View
        void UpdateData(TDto data);

        void Show();
    }
}
